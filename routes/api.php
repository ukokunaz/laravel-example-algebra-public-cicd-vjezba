<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\Customer;
use App\Models\Comment;
use App\Models\Post;
use App\Models\Student;

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:sanctum');

Route::get('/customer', function ()
{
    return Customer::fetchAll();
});

Route::get('/comments', function ()
{
    return Comment::with('post')->get();
});

Route::get('/posts', function ()
{
    return Post::with('comments')->get();
});

Route::get('/students', function ()
{
    return Student::with('courses')->get();
});

Route::get('/students', function ()
{
    return Student::with('courses')->get();
});