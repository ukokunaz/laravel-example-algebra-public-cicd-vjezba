<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Profile;

class Customer extends Model
{
    use HasFactory;

    public function profile()
    {
        return $this->hasOne(Profile::class);
    }

    public static function fetchAll()
    {
        $customers = self::all();

        // $data = [];
        // foreach ($customers as $customer)
        // {
        //    $customer['name'] = $customer->profile->name;
        //    $customer['surname'] = $customer->profile->surname;
        // }

        $customer = self::with('profile')->get();

        return $customers;
    }
}
