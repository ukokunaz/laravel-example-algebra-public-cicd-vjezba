<?php

namespace Database\Seeders;

use App\Models\User;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Profile;
use App\Models\Post;
use App\Models\Comment;
use App\Models\Student;
use App\Models\Course;
use App\Models\CourseStudent;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // User::factory(10)->create();

        // User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
        Profile::factory(20)->create();
        Post::factory(10)->create();
        Comment::factory(100)->create();
        Student::factory(10)->create();
        Course::factory(10)->create();
        // CourseStudent::factory(50)->create();
    }
}
